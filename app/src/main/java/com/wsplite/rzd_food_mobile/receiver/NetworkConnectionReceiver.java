package com.wsplite.rzd_food_mobile.receiver;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.wsplite.rzd_food_mobile.App;
import com.wsplite.rzd_food_mobile.R;
import com.wsplite.rzd_food_mobile.data.AppType;
import com.wsplite.rzd_food_mobile.ui.MainActivity;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static android.app.Notification.PRIORITY_MAX;
import static android.app.NotificationManager.IMPORTANCE_HIGH;
import static android.app.NotificationManager.IMPORTANCE_MAX;
import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Evgeniy Mezentsev on 2019-09-27.
 */
public class NetworkConnectionReceiver extends BroadcastReceiver {

    private static final int NOTIFICATION_ID = 0;

    private static final String CHANNEL_ID = "rzd_food_channel_id";

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mobile == null || wifi == null) return;

        if ((mobile.isConnected() || wifi.isConnected()) && AppType.INSTANCE.isOrder) {
            AppType.INSTANCE.isOrder = false;

            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);

            //Create the content intent for the notification, which launches this activity
            Intent contentIntent = new Intent(context, MainActivity.class);
            PendingIntent contentPendingIntent = PendingIntent.getActivity
                    (context, NOTIFICATION_ID, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            //Build the notification
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_kfc)
                    .setContentTitle("Title")
                    .setContentText("Text")
                    .setContentIntent(contentPendingIntent)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(NotificationCompat.DEFAULT_ALL);

            Toast.makeText(context, "Данные отправлены", Toast.LENGTH_LONG).show();

            //Deliver the notification
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        }
    }
}