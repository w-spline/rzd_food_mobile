package com.wsplite.rzd_food_mobile;

import android.app.Application;

import androidx.room.Room;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wsplite.rzd_food_mobile.data.db.AppDatabase;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public class App extends Application {

    public static App instance;

    private AppDatabase database;

    private Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(this, AppDatabase.class, "database")
                .allowMainThreadQueries()
                .build();
        gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

    }

    public static App getInstance() {
        return instance;
    }

    public AppDatabase getDatabase() {
        return database;
    }

    public Gson getGson() { return gson; }
}

