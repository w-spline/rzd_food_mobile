package com.wsplite.rzd_food_mobile.ui.restaurants;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wsplite.rzd_food_mobile.App;
import com.wsplite.rzd_food_mobile.R;
import com.wsplite.rzd_food_mobile.data.AppType;
import com.wsplite.rzd_food_mobile.ui.products.ProductItem;

import java.util.List;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public class RestaurantsRvAdapter extends RecyclerView.Adapter<RestaurantsRvAdapter.ViewHolder> {

    private List<RestaurantItem> mValues;
    private final RestaurantsFragment mListener;

    public RestaurantsRvAdapter(List<RestaurantItem> items, RestaurantsFragment listener) {
        mValues = items;
        mListener = listener;
    }

    public void setValues(List<RestaurantItem> values) {
        this.mValues = values;
        notifyDataSetChanged();
    }

    public void addValue(RestaurantItem value) {
        if (!this.mValues.contains(value)) {
            this.mValues.add(value);
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        Glide
                .with(holder.mView)
                .load(AppType.INSTANCE.url + mValues.get(position).image)
                .placeholder(R.drawable.ic_restaurant_default)
                .into(holder.ivImage);
        Glide
                .with(holder.mView)
                .load(AppType.INSTANCE.url + mValues.get(position).logo)
                .placeholder(R.drawable.ic_restaurant_default)
                .into(holder.ivLogo);
        holder.tvName.setText(mValues.get(position).name);
        holder.tvStation.setText(mValues.get(position).station);
        holder.tvAfterTime.setText(holder.mView.getContext().getString(R.string.after_time, mValues.get(position).afterTime));
        holder.tvMinCheck.setText(holder.mView.getContext().getString(R.string.min_check, mValues.get(position).minCheck));

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.clickRestaurant(mValues.get(position).id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final View mView;
        final CardView cardView;
        final TextView tvName;
        final ImageView ivImage;
        final ImageView ivLogo;
        final TextView tvAfterTime;
        final TextView tvStation;
        final TextView tvMinCheck;
        RestaurantItem mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            cardView = view.findViewById(R.id.card_view);
            tvName = view.findViewById(R.id.tv_name);
            ivImage = view.findViewById(R.id.iv_image);
            ivLogo = view.findViewById(R.id.iv_logo);
            tvAfterTime = view.findViewById(R.id.tv_after);
            tvStation = view.findViewById(R.id.tv_station);
            tvMinCheck = view.findViewById(R.id.tv_min_check);
        }
    }
}
