package com.wsplite.rzd_food_mobile.ui;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.wsplite.rzd_food_mobile.App;
import com.wsplite.rzd_food_mobile.R;
import com.wsplite.rzd_food_mobile.data.model.Basket;
import com.wsplite.rzd_food_mobile.receiver.NetworkConnectionReceiver;
import com.wsplite.rzd_food_mobile.ui.basket.BasketFragment;
import com.wsplite.rzd_food_mobile.ui.orders.OrdersFragment;
import com.wsplite.rzd_food_mobile.ui.restaurants.RestaurantsFragment;
import com.wsplite.rzd_food_mobile.ui.settings.SettingsFragment;
import com.wsplite.rzd_food_mobile.ui.tickets.TicketsFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static String TYPE = "type";

    public static final String RESTAURANTS = "RESTAURANTS";
    public static final String ORDERS = "ORDERS";
    public static final String BASKET = "BASKET";
    public static final String TICKETS = "TICKETS";
    public static final String SETTINGS = "SETTINGS";
    public static final int RESTAURANTS_INDEX = 0;
    public static final int ORDERS_INDEX = 1;
    public static final int BASKET_INDEX = 2;
    public static final int TICKETS_INDEX = 3;
    public static final int SETTINGS_INDEX = 4;

    @BindView(R.id.tv_toolbar_title) TextView tvToolbarTitle;
    @BindView(R.id.tv_toolbar_station) TextView tvToolbarStation;
    @BindView(R.id.bottom_navigation) AHBottomNavigation bottomNavigation;
    @BindView(R.id.ofline_mode) View offlineMode;

    private Fragment currentFragment;

    public static void start(Context context, int type) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(TYPE, type);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        setupBottomNavigation();
        int type = getIntent().getIntExtra(TYPE, RESTAURANTS_INDEX);
        onTabSelected(type, false);

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(new NetworkConnectionReceiver(), filter);

        //setupBasketDataInDb();
    }

    public void setupDefaultToolbar() {
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(R.string.app_name);
        tvToolbarStation.setVisibility(View.GONE);
    }

    public void setupToolbar(String name) {
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarStation.setVisibility(View.GONE);
        tvToolbarTitle.setText(name);
    }

    public void setupStationToolbar(String stationName) {
        tvToolbarStation.setText(getString(R.string.toolbar_station, stationName));
        tvToolbarTitle.setVisibility(View.GONE);
        tvToolbarStation.setVisibility(View.VISIBLE);
    }

    public void showOfflineMode() {
        offlineMode.setVisibility(View.VISIBLE);
    }

    public void hideOfflineMode() {
        offlineMode.setVisibility(View.GONE);
    }

    private void setupBottomNavigation() {
        bottomNavigation.setColored(false);
        bottomNavigation.setForceTint(true);
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setOnTabSelectedListener(this::onTabSelected);

        bottomNavigation.setAccentColor(ContextCompat.getColor(this, R.color.white));
        bottomNavigation.setInactiveColor(ContextCompat.getColor(this, R.color.light_white));

        bottomNavigation.setDefaultBackgroundColor(ContextCompat.getColor(this, R.color.colorMain));
        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);

        AHBottomNavigationAdapter navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.main_bottom_bar);
        navigationAdapter.setupWithBottomNavigation(bottomNavigation);
    }

    private boolean onTabSelected(int position, boolean wasSelected) {
        if (wasSelected) {
            return false;
        }
        switch (position) {
            case RESTAURANTS_INDEX:
                setupDefaultToolbar();
                showFragment(RESTAURANTS, RestaurantsFragment.newInstance());
                return true;
            case ORDERS_INDEX:
                showFragment(ORDERS, OrdersFragment.newInstance());
                return true;
            case BASKET_INDEX:
                setupToolbar(getString(R.string.toolbar_basket));
                showFragment(BASKET, BasketFragment.newInstance());
                return true;
            case TICKETS_INDEX:
                showFragment(TICKETS, TicketsFragment.newInstance());
                return true;
            case SETTINGS_INDEX:
                showFragment(SETTINGS, SettingsFragment.newInstance());
                return true;
            default:
                return false;
        }
    }

    private void showFragment(String tag, Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (currentFragment != null && currentFragment.isVisible()) {
            transaction.hide(currentFragment);
        }

        Fragment newFragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (newFragment == null) {
            newFragment = fragment;
            transaction.add(R.id.content_frame, newFragment, tag);
        }
        currentFragment = newFragment;

        transaction.show(newFragment);
        try {
            transaction.commitNow();
        } catch (IllegalStateException ignored) {
        }
    }

    private void setupBasketDataInDb() {
        String url = "https://www.restoran.ru/upload/resize_cache/iblock/82e/1000_667_1ecda9200b395d49b21a355839cb65e9f/img_33193b5.jpg";
        List<Basket> basketItemList = new ArrayList();
        for (int i = 0; i < 20; i++) {
            basketItemList.add(new Basket("" + i, "Burger" + i, url, 2, 450, 1));
        }

        try {
            App.getInstance().getDatabase().basketDao().insert(basketItemList);
        } catch (Exception ex) {

        }
    }
}
