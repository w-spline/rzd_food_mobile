package com.wsplite.rzd_food_mobile.ui.products;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public class ProductItem {

    public String id;
    public String name;
    public String image;
    public String description;
    public int price;
    public int count;
    public int cafeId;

    public ProductItem(String id, String name, String image, String description, int price, int cafeId) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.description = description;
        this.price = price;
        this.count = 0;
        this.cafeId = cafeId;
    }
}
