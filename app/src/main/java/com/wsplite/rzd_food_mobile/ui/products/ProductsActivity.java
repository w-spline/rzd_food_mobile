package com.wsplite.rzd_food_mobile.ui.products;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.wsplite.rzd_food_mobile.App;
import com.wsplite.rzd_food_mobile.R;
import com.wsplite.rzd_food_mobile.data.AppType;
import com.wsplite.rzd_food_mobile.data.api.ApiFactory;
import com.wsplite.rzd_food_mobile.data.model.Basket;
import com.wsplite.rzd_food_mobile.data.model.Menu;
import com.wsplite.rzd_food_mobile.ui.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ProductsActivity extends AppCompatActivity {

    private static String ID = "id";

    public static void start(Context context, String id) {
        Intent intent = new Intent(context, ProductsActivity.class);
        intent.putExtra(ID, id);
        context.startActivity(intent);
    }

    private Map<String, ProductItem> productItems = new HashMap<>();

    private ProductsRvAdapter adapter;

    @BindView(R.id.iv_image) ImageView toolbarImageView;
    @BindView(R.id.tv_toolbar_title) TextView tvToolbarTitle;

    @BindView(R.id.apl_products) AppBarLayout appBarLayout;
    @BindView(R.id.ctl_group_chat_info) CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.tlb_name) Toolbar toolbar;

    @BindView(R.id.rl_products) RecyclerView rlProducts;
    @BindView(R.id.btn_select_basket) Button selectBasket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        ButterKnife.bind(this, this);
        Toolbar toolbar = findViewById(R.id.toolbar);

        //generateTestData();

        setupAdapters();
        setupToolbar();
        if (AppType.INSTANCE.isOffline) {
            setupOfflineRestaurantData(getIntent().getStringExtra(ID));
            setupOfflineData(getIntent().getStringExtra(ID));
        } else {
            setupRestaurantData(getIntent().getStringExtra(ID));
            setupData(getIntent().getStringExtra(ID));
        }
        selectBasket.setOnClickListener(view -> {
            MainActivity.start(this, MainActivity.BASKET_INDEX);
        });


        setSupportActionBar(toolbar);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(this, R.color.transparent));
    }

    public void showRestaurantName(String restaurantName) {
        collapsingToolbarLayout.setTitle(restaurantName);
        tvToolbarTitle.setText(restaurantName);
    }

    public void showImageRestaurant(String url) {
        Glide
                .with(this)
                .load(AppType.INSTANCE.url + url)
                .placeholder(R.drawable.ic_restaurant_default)
                .into(toolbarImageView);
    }

    public void changeProduct(ProductItem productItem) {
        if (productItem.count == 0) {
            productItems.remove(productItem.id);
            productItems.put(productItem.id, productItem);
            try {
                App.getInstance().getDatabase().basketDao().delete(productItem.id);
            } catch (Exception ex) {

            }
        } else {
            if (productItems.containsKey(productItem.id)) {
                try {
                    App.getInstance().getDatabase().basketDao().update(new Basket(productItem.id, productItem.name,
                            productItem.image, productItem.count, productItem.price, productItem.cafeId));
                } catch (Exception ex) {
                    Log.e("TAG", ex.getMessage());
                }
            } else {
                try {
                    App.getInstance().getDatabase().basketDao().insert(new Basket(productItem.id, productItem.name,
                            productItem.image, productItem.count, productItem.price, productItem.cafeId));
                } catch (Exception ex) {
                    Log.e("TAG", ex.getMessage());
                }
            }
            productItems.put(productItem.id, productItem);
        }
        if (productItems.isEmpty()) {
            selectBasket.setVisibility(View.GONE);
        } else {
            selectBasket.setVisibility(View.VISIBLE);
        }
    }

    private void setupAdapters() {

        adapter = new ProductsRvAdapter(new ArrayList<>(), this);
        rlProducts.setLayoutManager(new LinearLayoutManager(this));
        rlProducts.setAdapter(adapter);
    }

    private void setupData(String cafeId) {
        Disposable disposable = ApiFactory.INSTANCE.getGeneralApi()
                .getMenu(cafeId)
                .doOnNext(items -> {
                    try {
                        App.getInstance().getDatabase().menuDao().insert(items);
                    } catch (Exception ex) {

                    }
                })
                .flatMap(Observable::fromIterable)
                .map(item -> new ProductItem(
                        item.foodId,
                        item.foodName,
                        item.foodImage,
                        item.description,
                        item.price,
                        Integer.valueOf(item.cafeId)
                ))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> adapter.addValue(list), error -> Log.d("TAG", error.getMessage()));
    }

    private void setupOfflineData(String cafeId) {
        adapter.setValues(new ArrayList<>());
        Disposable disposable = App.getInstance().getDatabase().menuDao().getById(Integer.valueOf(cafeId))
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(item -> new ProductItem(
                        item.foodId,
                        item.foodName,
                        item.foodImage,
                        item.description,
                        item.price,
                        Integer.valueOf(item.cafeId)
                ))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> adapter.addValue(list), error -> Log.d("TAG", error.getMessage()));
    }

    private void setupRestaurantData(String cafeId) {
        Disposable disposable = App.getInstance().getDatabase().restaurantsDao().getById(Integer.valueOf(cafeId))
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(item -> {
                    showRestaurantName(item.cafeName);
                    showImageRestaurant(item.image);
                }, error -> Log.d("TAG", error.getMessage()));
    }

    private void setupOfflineRestaurantData(String cafeId) {
        Disposable disposable = App.getInstance().getDatabase().restaurantsDao().getById(Integer.valueOf(cafeId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(item -> {
                    showRestaurantName(item.cafeName);
                    showImageRestaurant(item.image);
                }, error -> Log.d("TAG", error.getMessage()));
    }

    private void generateTestData() {
        String url = "https://www.restoran.ru/upload/resize_cache/iblock/82e" +
                "/1000_667_1ecda9200b395d49b21a355839cb65e9f/img_33193b5.jpg";
        List<Menu> productItemList = new ArrayList();
        for (int i = 0; i < 20; i++) {
            productItemList.add(new Menu("" + i, "Бургер " + i, "Description " + i, 450, "" + 0, "name", "" + i,
                    "name", url));
        }

        try {
            App.getInstance().getDatabase().menuDao().insert(productItemList);
        } catch (Exception ex) {

        }
    }

    private void showTestTitle() {
        showRestaurantName("Burger King");
        String url = "https://www.restoran.ru/upload/resize_cache/iblock/82e/1000_667_1ecda9200b395d49b21a355839cb65e9f/img_33193b5.jpg";
        showImageRestaurant(url);
    }
}
