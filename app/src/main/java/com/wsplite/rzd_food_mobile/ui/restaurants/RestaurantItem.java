package com.wsplite.rzd_food_mobile.ui.restaurants;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public class RestaurantItem {

    public String id;
    public String name;
    public String image;
    public String logo;
    public String station;
    public String afterTime;
    public String minCheck;

    public RestaurantItem(String id, String name, String image, String logo, String station, String afterTime, String minCheck) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.logo = logo;
        this.station = station;
        this.afterTime = afterTime;
        this.minCheck = minCheck;
    }
}
