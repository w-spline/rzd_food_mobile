package com.wsplite.rzd_food_mobile.ui.basket;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public class BasketItem {

    String id;
    String name;
    String image;
    int qty;
    int price;
    int cafeId;

    public BasketItem(String id, String name, String image, int qty, int price, int cafeId) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.qty = qty;
        this.price = price;
        this.cafeId = cafeId;
    }
}
