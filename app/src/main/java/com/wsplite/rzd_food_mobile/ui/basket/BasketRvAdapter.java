package com.wsplite.rzd_food_mobile.ui.basket;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wsplite.rzd_food_mobile.R;
import com.wsplite.rzd_food_mobile.data.AppType;

import java.util.List;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public class BasketRvAdapter extends RecyclerView.Adapter<BasketRvAdapter.BasketVH> {

    private List<BasketItem> mValues;
    private final BasketFragment mListener;

    public BasketRvAdapter(List<BasketItem> items, BasketFragment listener) {
        mValues = items;
        mListener = listener;
    }

    public void setValues(List<BasketItem> values) {
        this.mValues = values;
        notifyDataSetChanged();
    }

    public void addValue(BasketItem value) {
        this.mValues.add(value);
        notifyDataSetChanged();
    }

    @Override
    public BasketRvAdapter.BasketVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.basket_item, parent, false);
        return new BasketRvAdapter.BasketVH(view);
    }

    @Override
    public void onBindViewHolder(final BasketRvAdapter.BasketVH holder, int position) {
        holder.mItem = mValues.get(position);
        Glide
                .with(holder.mView)
                .load(AppType.INSTANCE.url + mValues.get(position).image)
                .placeholder(R.drawable.ic_restaurant_default)
                .into(holder.ivImage);
        holder.tvName.setText(mValues.get(position).name);
        holder.tvQty.setText(holder.itemView.getContext().getString(R.string.qty, Integer.toString(
                mValues.get(position).qty)));
        holder.tvPrice.setText(holder.itemView.getContext().getString(R.string.price, Integer.toString(
                mValues.get(position).price)));

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class BasketVH extends RecyclerView.ViewHolder {

        final View mView;
        final TextView tvName;
        final ImageView ivImage;
        final TextView tvQty;
        final TextView tvPrice;
        BasketItem mItem;

        BasketVH(View view) {
            super(view);
            mView = view;
            tvName = view.findViewById(R.id.tv_name_item);
            ivImage = view.findViewById(R.id.iv_item);
            tvQty = view.findViewById(R.id.tv_qty_item);
            tvPrice = view.findViewById(R.id.tv_price_item);
        }
    }
}
