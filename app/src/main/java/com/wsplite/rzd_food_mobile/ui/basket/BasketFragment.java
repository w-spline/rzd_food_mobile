package com.wsplite.rzd_food_mobile.ui.basket;

import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wsplite.rzd_food_mobile.App;
import com.wsplite.rzd_food_mobile.R;
import com.wsplite.rzd_food_mobile.data.AppType;
import com.wsplite.rzd_food_mobile.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BasketFragment extends Fragment {

    public static BasketFragment newInstance() {
        BasketFragment fragment = new BasketFragment();
        return fragment;
    }

    @BindView(R.id.card_view) CardView cardView;
    @BindView(R.id.nestedScrollView) NestedScrollView nestedScrollView;
    @BindView(R.id.empty_item) View emptyItem;

    @BindView(R.id.iv_logo) ImageView ivLogo;
    @BindView(R.id.tv_restaurant_name) TextView tvRestaurantName;

    @BindView(R.id.tv_delivery) TextView tvDelivery;
    @BindView(R.id.tv_price) TextView tvPrice;
    @BindView(R.id.btn_select_basket) Button btnSelectBasket;

    @BindView(R.id.rl_basket) RecyclerView rlBasket;
    BasketRvAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basket, container, false);
        ButterKnife.bind(this, view);

        setupAdapters();
        setupData();

        btnSelectBasket.setOnClickListener(view1 -> {
            TextView tv = (TextView) LayoutInflater.from(this.requireContext())
                    .inflate(R.layout.view_alert_dialog, null, false);
            int message;
            if (AppType.INSTANCE.isOffline) {
                message = R.string.alert_offline_send;
            } else {
                message = R.string.alert_send;
            }
            tv.setText(message);
            new AlertDialog.Builder(requireContext())
                    .setTitle(R.string.title_order)
                    .setView(tv)
                    .setPositiveButton(R.string.ok, (dialogInterface, i) -> dialogInterface.dismiss())
                    .create()
                    .show();
            AppType.INSTANCE.isOrder = true;
            try {
                App.getInstance().getDatabase().basketDao().delete();
                setupData();
            } catch (Exception ex) {

            }
        });

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupToolbar();
    }

    public void setupToolbar() {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).setupToolbar((getString(R.string.toolbar_basket)));
        }
    }

    public void setLogo(String url) {
        Glide
                .with(this)
                .load(url)
                .placeholder(R.drawable.ic_kfc)
                .into(ivLogo);
    }

    public void setDelivery(int delivery) {
        if (delivery == 0) {
            tvDelivery.setText(getString(R.string.delivery_free));
        } else {
            tvDelivery.setText(getString(R.string.delivery_price, delivery));
        }
    }

    public void setFullPrice(int price) {
        if (price == 0) {
            tvPrice.setText(getString(R.string.full_price_free));
        } else {
            tvPrice.setText(getString(R.string.full_price, price));
        }
    }

    private void setupAdapters() {

        adapter = new BasketRvAdapter(new ArrayList<>(), this);
        rlBasket.setLayoutManager(new LinearLayoutManager(this.requireContext()));
        rlBasket.setAdapter(adapter);
    }

    public int price;

    private void setupData() {
        adapter.setValues(new ArrayList<>());
        cardView.setVisibility(View.GONE);
        nestedScrollView.setVisibility(View.GONE);
        emptyItem.setVisibility(View.VISIBLE);
        Disposable disposable = App.getInstance().getDatabase().basketDao()
                .getAll()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(item -> new BasketItem(item.id, item.name, item.image, item.qty, item.price, item.cafeId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    price += list.price;
                    cardView.setVisibility(View.VISIBLE);
                    nestedScrollView.setVisibility(View.VISIBLE);
                    emptyItem.setVisibility(View.GONE);
                    adapter.addValue(list);
                    setDelivery(0);
                    setFullPrice(price);
                    setupRestaurantData(list.cafeId);
                }, error -> Log.d("TAG", error.getMessage()));
    }

    private void setupRestaurantData(int cafeId) {
        if (tvRestaurantName.getText() != null && !tvRestaurantName.getText().equals("")) return;
        Disposable disposable = App.getInstance().getDatabase().restaurantsDao().getById(cafeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(item -> {
                    tvRestaurantName.setText(item.cafeName);
                    setLogo(item.logo);
                }, error -> Log.d("TAG", error.getMessage()));
    }

    private void geterateTestData() {
        String url = "https://www.restoran.ru/upload/resize_cache/iblock/82e/1000_667_1ecda9200b395d49b21a355839cb65e9f/img_33193b5.jpg";
        List<BasketItem> basketItemList = new ArrayList();
        for (int i = 0; i < 20; i++) {
            basketItemList.add(new BasketItem("" + i, "Burger" + i, url, 2, 450, 0));
        }

    }
}
