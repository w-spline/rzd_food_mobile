package com.wsplite.rzd_food_mobile.ui.products;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.wsplite.rzd_food_mobile.R;
import com.wsplite.rzd_food_mobile.data.AppType;

import java.util.List;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public class ProductsRvAdapter extends RecyclerView.Adapter<ProductsRvAdapter.ProductVH> {

    private List<ProductItem> mValues;
    private final ProductsActivity mListener;

    public ProductsRvAdapter(List<ProductItem> items, ProductsActivity listener) {
        mValues = items;
        mListener = listener;
    }

    public void setValues(List<ProductItem> values) {
        this.mValues = values;
        notifyDataSetChanged();
    }

    public void addValue(ProductItem value) {
        this.mValues.add(value);
        notifyDataSetChanged();
    }

    @Override
    public ProductsRvAdapter.ProductVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item, parent, false);
        return new ProductsRvAdapter.ProductVH(view);
    }

    @Override
    public void onBindViewHolder(final ProductsRvAdapter.ProductVH holder, int position) {
        holder.mItem = mValues.get(position);
        Glide
                .with(holder.mView)
                .load(AppType.INSTANCE.url + mValues.get(position).image)
                .placeholder(R.drawable.ic_restaurant_default)
                .into(holder.ivImage);
        holder.tvName.setText(mValues.get(position).name);
        holder.tvDescription.setText(mValues.get(position).description);
        holder.tvPrice.setText(holder.itemView.getContext().getString(R.string.price,
                Integer.toString(mValues.get(position).price)));
        holder.enmProduct.setOnClickListener((ElegantNumberButton.OnClickListener) view -> {
            mValues.get(position).count = Integer.valueOf(holder.enmProduct.getNumber());
            mListener.changeProduct(mValues.get(position));
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ProductVH extends RecyclerView.ViewHolder {

        final View mView;
        final TextView tvName;
        final ImageView ivLogo;
        final ImageView ivImage;
        final TextView tvDescription;
        final TextView tvPrice;
        final ElegantNumberButton enmProduct;
        ProductItem mItem;

        ProductVH(View view) {
            super(view);
            mView = view;
            tvName = view.findViewById(R.id.tv_name);
            ivLogo = view.findViewById(R.id.iv_logo);
            ivImage = view.findViewById(R.id.iv_image);
            tvDescription = view.findViewById(R.id.tv_description);
            tvPrice = view.findViewById(R.id.tv_price);
            enmProduct = view.findViewById(R.id.enm_product);
        }
    }
}
