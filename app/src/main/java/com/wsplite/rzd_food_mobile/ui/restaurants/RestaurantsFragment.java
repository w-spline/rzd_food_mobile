package com.wsplite.rzd_food_mobile.ui.restaurants;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wsplite.rzd_food_mobile.App;
import com.wsplite.rzd_food_mobile.R;
import com.wsplite.rzd_food_mobile.data.AppType;
import com.wsplite.rzd_food_mobile.data.api.ApiFactory;
import com.wsplite.rzd_food_mobile.data.model.Restaurants;
import com.wsplite.rzd_food_mobile.ui.MainActivity;
import com.wsplite.rzd_food_mobile.ui.products.ProductsActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by Evgeniy Mezentsev on 2019-09-27.
 */
public class RestaurantsFragment extends Fragment {

    public static RestaurantsFragment newInstance() {
        RestaurantsFragment fragment = new RestaurantsFragment();
        return fragment;
    }

    @BindView(R.id.wifi_connected) View wifiConnected;
    @BindView(R.id.btn_select_basket) Button btnWifiConnected;

    @BindView(R.id.rl_restaurants) RecyclerView rlRestaurants;
    RestaurantsRvAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurants, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);

        setupAdapters();
        setupListeners();
        setupData();

        //generateTestData();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        showScreen();
        setupToolbar("Гавайский сад");
    }

    public void setupToolbar(String stationName) {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).setupStationToolbar(stationName);
        }
    }

    public void clickRestaurant(String id) {
        ProductsActivity.start(this.requireContext(), id);
    }

    private void setupAdapters() {

        adapter = new RestaurantsRvAdapter(new ArrayList<>(), this);
        rlRestaurants.setLayoutManager(new LinearLayoutManager(this.requireContext()));
        rlRestaurants.setAdapter(adapter);
    }

    private void setupData() {
        adapter.setValues(new ArrayList());
        Disposable disposable = ApiFactory.INSTANCE.getGeneralApi()
                .getRestaurants(App.getInstance().getGson().toJson(new Date()).replace("\"", ""))
                .doOnNext(items -> {
                    try {
                        App.getInstance().getDatabase().restaurantsDao().insert(items);
                    } catch (Exception ex) {

                    }
                })
                .flatMap(Observable::fromIterable)
                .map(restaurant -> new RestaurantItem(
                        Integer.toString(restaurant.cafeId),
                        restaurant.cafeName,
                        restaurant.image,
                        restaurant.logo,
                        restaurant.stationName,
                        restaurant.deliveryTime,
                        Integer.toString(restaurant.minPrice)
                ))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> adapter.addValue(list), error -> Log.d("TAG", error.getMessage()));
    }

    private void setupOfflineData() {
        adapter.setValues(new ArrayList());
        Disposable disposable = App.getInstance().getDatabase().restaurantsDao().getAll()
                .toObservable()
                .flatMap(Observable::fromIterable)
                .map(restaurant -> new RestaurantItem(
                        Integer.toString(restaurant.cafeId),
                        restaurant.cafeName,
                        restaurant.image,
                        restaurant.logo,
                        restaurant.stationName,
                        restaurant.deliveryTime,
                        Integer.toString(restaurant.minPrice)
                ))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> adapter.addValue(list), error -> Log.d("TAG", error.getMessage()));
    }

    private String parseSeconds(int seconds) {
        if (seconds < 60) {
            return getString(R.string.seconds, seconds);
        } else if (seconds < 3600) {
            return getString(R.string.minutes, seconds / 60);
        } else if (seconds < 86400) {
            return getString(R.string.hours, seconds / 3600);
        } else {
            return getString(R.string.seconds, seconds);
        }
    }

    private void showScreen() {
        if (checkConnection()) {
            ((MainActivity) getActivity()).hideOfflineMode();
            rlRestaurants.setVisibility(View.VISIBLE);
            wifiConnected.setVisibility(View.GONE);
        } else if (AppType.INSTANCE.isOffline) {
            setupOfflineData();
            rlRestaurants.setVisibility(View.VISIBLE);
            wifiConnected.setVisibility(View.GONE);
            ((MainActivity) getActivity()).showOfflineMode();
        } else {
            ((MainActivity) getActivity()).hideOfflineMode();
            wifiConnected.setVisibility(View.VISIBLE);
        }
    }

    private void setupListeners() {
        btnWifiConnected.setOnClickListener(view -> {
            AppType.INSTANCE.isOffline = true;
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        });
    }

    private boolean checkConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) this.requireContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork == null) {
            return false;
        } else {
            return activeNetwork.isConnected();
        }
    }

    private void generateTestData() {
        String url = "https://www.restoran.ru/upload/resize_cache/iblock/82e/1000_667_1ecda9200b395d49b21a355839cb65e9f/img_33193b5.jpg";
        String logo = "https://upload.wikimedia.org/wikipedia/ru/thumb/b/bf/KFC_logo.svg/1200px-KFC_logo.svg.png";
        List<Restaurants> restaurantItemList = new ArrayList();
        for (int i = 0; i < 20; i++) {
            restaurantItemList.add(new Restaurants(i, "Ресторан" + i, "desc", url, logo, 450, "45 мин",
                    "Station" + i));
        }

        try {
            App.getInstance().getDatabase().restaurantsDao().insert(restaurantItemList);
        } catch (Exception ex) {

        }
    }
}
