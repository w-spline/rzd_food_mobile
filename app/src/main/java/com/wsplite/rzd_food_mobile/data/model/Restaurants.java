package com.wsplite.rzd_food_mobile.data.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
@Entity
public class Restaurants {

    @PrimaryKey
    @SerializedName("id")
    public int cafeId;
    @SerializedName("name")
    public String cafeName;
	@SerializedName("description")
    public String description;
	@SerializedName("logo")
    public String logo;
	@SerializedName("image")
    public String image;
	@SerializedName("min_price")
    public int minPrice;
	@SerializedName("delivery_time")
    public String deliveryTime;
	@SerializedName("station_name")
    public String stationName;

    public Restaurants(int cafeId, String cafeName, String description,
                       String logo, String image, int minPrice, String deliveryTime,
                       String stationName) {
        this.cafeId = cafeId;
        this.cafeName = cafeName;
        this.description = description;
        this.logo = logo;
        this.image = image;
        this.minPrice = minPrice;
        this.deliveryTime = deliveryTime;
        this.stationName = stationName;
    }
}
