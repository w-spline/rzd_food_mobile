package com.wsplite.rzd_food_mobile.data.api;

import com.wsplite.rzd_food_mobile.data.model.Menu;
import com.wsplite.rzd_food_mobile.data.model.OrderRequest;
import com.wsplite.rzd_food_mobile.data.model.Restaurants;

import java.util.Date;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public interface GeneralApi {

    @GET("/api/cafes/by_train_line/1")
    Observable<List<Restaurants>> getRestaurants(@Query("date") String date);

    @GET("/api/cafes/menu/{cafe_id}")
    Observable<List<Menu>> getMenu(@Path("cafe_id") String cafeId);

    @POST("/api/orders")
    Completable sendOrder(@Body OrderRequest request);
}
