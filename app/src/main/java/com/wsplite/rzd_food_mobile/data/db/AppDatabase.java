package com.wsplite.rzd_food_mobile.data.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.wsplite.rzd_food_mobile.data.db.dao.BasketDao;
import com.wsplite.rzd_food_mobile.data.db.dao.MenuDao;
import com.wsplite.rzd_food_mobile.data.db.dao.RestaurantsDao;
import com.wsplite.rzd_food_mobile.data.model.Basket;
import com.wsplite.rzd_food_mobile.data.model.Menu;
import com.wsplite.rzd_food_mobile.data.model.Restaurants;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
@Database(entities = {Menu.class, Restaurants.class, Basket.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract RestaurantsDao restaurantsDao();
    public abstract MenuDao menuDao();
    public abstract BasketDao basketDao();
}