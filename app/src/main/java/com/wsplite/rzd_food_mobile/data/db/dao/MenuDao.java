package com.wsplite.rzd_food_mobile.data.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.wsplite.rzd_food_mobile.data.model.Menu;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
@Dao
public interface MenuDao {

    @Query("SELECT * FROM menu")
    Flowable<List<Menu>> getAll();

    @Query("SELECT * FROM menu WHERE cafeId = :id")
    Flowable<List<Menu>> getById(int id);

    @Insert
    void insert(Menu menu);

    @Insert
    void insert(List<Menu> menus);

    @Update
    void update(Menu menu);

    @Delete
    void delete(Menu menu);
}
