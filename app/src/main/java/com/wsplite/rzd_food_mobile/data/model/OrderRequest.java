package com.wsplite.rzd_food_mobile.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public class OrderRequest {

    @SerializedName("user_id")
    public String userId;
    @SerializedName("vagon_num")
    public int vagonNum;
    @SerializedName("order_list")
    public List<Order> orderList;

    public OrderRequest(String userId, int vagonNum, List<Order> orderList) {
        this.userId = userId;
        this.vagonNum = vagonNum;
        this.orderList = orderList;
    }

    class Order {

        @SerializedName("food_id")
        public int foodId;
        @SerializedName("qty")
        public int qty;

        public Order(int foodId, int qty) {
            this.foodId = foodId;
            this.qty = qty;
        }
    }
}
