package com.wsplite.rzd_food_mobile.data.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.wsplite.rzd_food_mobile.data.model.Basket;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
@Dao
public interface BasketDao {

    @Query("SELECT * FROM basket")
    Flowable<List<Basket>> getAll();

    @Query("SELECT * FROM basket WHERE id = :id")
    Flowable<Basket> getById(int id);

    @Insert
    void insert(Basket basket);

    @Insert
    void insert(List<Basket> baskets);

    @Update
    void update(Basket basket);

    @Delete
    void delete(Basket basket);

    @Query("DELETE FROM basket WHERE id = :id")
    void delete(String id);

    @Query("DELETE FROM basket")
    void delete();
}
