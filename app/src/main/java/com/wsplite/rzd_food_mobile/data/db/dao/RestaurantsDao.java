package com.wsplite.rzd_food_mobile.data.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.wsplite.rzd_food_mobile.data.model.Menu;
import com.wsplite.rzd_food_mobile.data.model.Restaurants;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
@Dao
public interface RestaurantsDao {

    @Query("SELECT * FROM restaurants")
    Flowable<List<Restaurants>> getAll();

    @Query("SELECT * FROM restaurants WHERE cafeId = :id")
    Flowable<Restaurants> getById(int id);

    @Insert
    void insert(Restaurants restaurants);
    
    @Insert
    void insert(List<Restaurants> restaurants);

    @Update
    void update(Restaurants restaurants);

    @Delete
    void delete(Restaurants restaurants);
}
