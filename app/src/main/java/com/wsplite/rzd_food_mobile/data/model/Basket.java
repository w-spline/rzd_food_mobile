package com.wsplite.rzd_food_mobile.data.model;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
@Entity
public class Basket {

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    public String id;
    @SerializedName("name")
    public String name;
    @SerializedName("image")
    public String image;
    @SerializedName("qty")
    public int qty;
    @SerializedName("price")
    public int price;
    @SerializedName("cafeId")
    public int cafeId;

    public Basket(String id, String name, String image, int qty, int price, int cafeId) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.qty = qty;
        this.price = price;
        this.cafeId = cafeId;
    }
}
