package com.wsplite.rzd_food_mobile.data;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public enum AppType {

    INSTANCE;

    public boolean isOffline = false;
    public boolean isOrder = false;
    public String url = "http://localhost:8888";

}
