package com.wsplite.rzd_food_mobile.data.api;

import com.google.gson.GsonBuilder;
import com.wsplite.rzd_food_mobile.data.AppType;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
public enum ApiFactory {

    INSTANCE;

    private final GeneralApi generalApi;

    ApiFactory() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppType.INSTANCE.url)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                        .create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        generalApi = retrofit.create(GeneralApi.class);
    }

    public GeneralApi getGeneralApi() {
        return generalApi;
    }
}
