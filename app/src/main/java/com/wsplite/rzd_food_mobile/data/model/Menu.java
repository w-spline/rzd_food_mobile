package com.wsplite.rzd_food_mobile.data.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Evgeniy Mezentsev on 2019-09-28.
 */
@Entity
public class Menu {

    @PrimaryKey
    @Nullable
    @NonNull
    @SerializedName("id")
    public String foodId;
    @SerializedName("name")
    public String foodName;
    @SerializedName("description")
    public String description;
    @SerializedName("price")
    public Integer price;
    @SerializedName("cafe")
    public String cafeId;
    @SerializedName("cafe_name")
    public String cafeName;
    @SerializedName("station")
    public String trainId;
    @SerializedName("train_name")
    public String trainName;
    @SerializedName("food_types")
    @Ignore
    public List<FoodType> foodTypeList;
    @SerializedName("image")
    public String foodImage;

    public Menu(String foodId, String foodName, String description, int price, String cafeId, String cafeName, String trainId, String trainName, List<FoodType> foodTypeList, String foodImage) {
        this.foodId = foodId;
        this.foodName = foodName;
        this.description = description;
        this.price = price;
        this.cafeId = cafeId;
        this.cafeName = cafeName;
        this.trainId = trainId;
        this.trainName = trainName;
        this.foodTypeList = foodTypeList;
        this.foodImage = foodImage;
    }

    public Menu(String foodId, String foodName, String description, int price, String cafeId, String cafeName, String trainId, String trainName, String foodImage) {
        this.foodId = foodId;
        this.foodName = foodName;
        this.description = description;
        this.price = price;
        this.cafeId = cafeId;
        this.cafeName = cafeName;
        this.trainId = trainId;
        this.trainName = trainName;
        this.foodImage = foodImage;
    }


    class FoodType {
        @SerializedName("food_type_id")
        public String foodTypeId;
        @SerializedName("food_type_name")
        public String foodTypeName;

        public FoodType(String foodTypeId, String foodTypeName) {
            this.foodTypeId = foodTypeId;
            this.foodTypeName = foodTypeName;
        }
    }
}
